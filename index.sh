set -e

GREEN='\033[0;32m'
Blue='\033[0;34m'
NC='\033[0m' # No Color

# external source fetch hosts files
get_latest_hosts() {
  FILE_PATH="hosts/_domains.txt"

  declare -a SRC_LIST=(
    "https://threatfox.abuse.ch/downloads/hostfile"    
    "https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/thirdparties/pgl.yoyo.org/as/serverlist"
    "https://raw.githubusercontent.com/blocklistproject/Lists/master/tracking.txt"
    "https://raw.githubusercontent.com/blocklistproject/Lists/master/ransomware.txt"
    "https://raw.githubusercontent.com/blocklistproject/Lists/master/scam.txt"
    "https://raw.githubusercontent.com/blocklistproject/Lists/master/torrent.txt"
    "https://raw.githubusercontent.com/blocklistproject/Lists/master/malware.txt"
    "https://raw.githubusercontent.com/cbuijs/accomplist/master/deugniets/black.top.ranked.list"
  )

    core2URL='https://raw.githubusercontent.com/t0ny54/core2/master/lists/'
      
    for ((i=1;i<62;i+=1)); do
        if [[ $i -lt 10 ]] 
        then
            num="0$i-justdomains.txt"
        else
            num="$i-justdomains.txt"
        fi
        core2List+=("$core2URL$num")
    done

    SRC_LIST+=( "${core2List[@]}" )

  # SRC_LIST_LEN=${#SRC_LIST[@]}

  for url in "${SRC_LIST[@]}"; do

    curl -s GET -H "Accept: text/plain" "$url" >>$FILE_PATH

    echo -e "> $GREEN Fetching: $url $NC"

    sed -i -E 's/address=\/|\/(0.0.0.0|::)//g; s/\|\||\^\$.*|\^.*|0.0.0.0\s*|127.0.0.1\s*//g;s/#.*$//g' $FILE_PATH
    sed -i -E 's/\$(script|third-party|popup).*//g' $FILE_PATH
  done
}

# remove invalid ip v4
remove_invalid_ip_v4() {
  ipRegExp="/\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}/!d"
  ips_dir=("ips")
  ips_files=$(find "${ips_dir[@]}" -type f -name '*.txt')

  for file in $ips_files; do
    sed -i $ipRegExp $file
    echo -e "> $GREEN Remove invalid ip v4: $file $NC"
  done
}

# remove duplicates and empty lines from all .txt files
clean_up() {
  domains_dirs=("netfilter" "lite-netfilter" "hosts" "ips")
  text_files=$(find "${domains_dirs[@]}" -type f -name '*.txt')
  for file in $text_files; do
    sed -i 's/^ *//; s/ *$//; /^$/d; /^\s*$/d' $file
    awk -i inplace '!seen[$0]++' $file
    echo -e "> $Blue Clean up: $file $NC"
  done
}

get_latest_hosts
remove_invalid_ip_v4
clean_up

# push files to Gitlab
printf 'Push changes to Gitlab (y/n)? '
read answer

if [ "$answer" != "${answer#[Yy]}" ]; then
  git add . && git commit -m "$(date +%F)" && git push
else
  exit
fi
